FROM php:7.4-apache
RUN apt-get update && apt-get install -y libtidy-dev libxml2-dev
RUN docker-php-ext-install pdo pdo_mysql tidy dom
RUN a2enmod rewrite
