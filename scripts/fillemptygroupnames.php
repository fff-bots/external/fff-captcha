<?php
   $arrContextOptions = stream_context_create(["ssl"=>["verify_peer"=>false,"verify_peer_name"=>false]]);
   require_once('./sys-files/sql.php');
   $sql = sql_connect();
   if(!is_null($sql)){
      $i = 0;
      $missing_ogs = select($sql,'SELECT * FROM links, ogs WHERE((links.group_name = null OR links.group_name = "") AND links.og_id = og.og_id)',[]);
      if(!is_null($missing_ogs)){
         foreach($missing_ogs as $missing_og){
            $link = 'https://api.fffutu.re/v1/localGroups/?name='.urlencode($missing_og['og']);
            $entry = json_decode(file_get_contents($link,false,$arrContextOptions),true)[0];
            if(!empty($entry['name']) && !empty($entry['state']) && (!empty($entry['whatsapp']))){
               if(!empty($entry['whatsapp'])){
                  $path = str_ireplace('whatsapp','whatsapp',$entry['whatsapp']);
                  $opts = [
                     'http' => [
                        'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0'
                     ]
                  ];
                  $context = stream_context_create($opts);
                  $html =  file_get_contents($path,false,$context);
                  $config = [
                     'clean' => 'yes',
                     'output-html' => 'yes',
                  ];
                  $tidy = new Tidy();
                  $tidy -> parseString($html,$config,'utf8');
                  unset($html);
                  $tidy->cleanRepair();
                  $dom = new DOMDocument;
                  @$dom->loadHTML($tidy);
                  unset($tidy);
                  @$heading2 = $dom -> getElementsByTagName('h2') -> item(0) -> nodeValue;
                  unset($dom);
                  $groupName = $heading2;
                  unset($heading2);
                  if(!empty($groupName)){
                     insert($sql,'UPDATE links set(link = :link, group_name = :group_name) WHERE(link_id = :link_id)',[':link_id' => $missing_og['link_id'],':link' => $entry['whatsapp'],':group_name' => $groupName]);
                     echo $missing_og['og'].'<br>';
                     $i++;
                  }
               }
            }
         }
      }
      echo "$i Einträge aktualisiert";
   }
?>