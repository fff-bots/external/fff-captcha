<?php
   function get_whatsapp_name($link){
      try{
         $path = str_ireplace('whatsapp','whatsapp',$link);
         $opts = [
         'http' => [
            'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0'
            ]
         ];
         $context = stream_context_create($opts);
         $html =  file_get_contents($path,false,$context);
         if($html === false){
            return null;
         }
         $config = [
            'clean' => 'yes',
            'output-html' => 'yes',
         ];
         $tidy = new tidy;
         $tidy -> parseString($html,$config,'utf8');
         unset($html);
         $tidy->cleanRepair();
         $dom = new DOMDocument;
         @$dom->loadHTML($tidy);
         unset($tidy);
         @$heading2 = $dom -> getElementsByTagName('h2') -> item(0) -> nodeValue;
         unset($dom);
         if(!empty($heading2)){
            return $heading2;
         }
         else{
            return null;
         }
      }
      catch(Exception $e){
         return null;
     }
   }
?>