<?php
   function sql_connect(){
      try{
         $server = 'localhost';
         $user = 'root';
         $password = '';
         $database = 'fff_captcha';
         $db = new PDO('mysql:host='.$server.';dbname='.$database,$user,$password);
         return $db;
      }
      catch(Exception $e){
         return null;
      }
   }
   function select($db,$statement,$variables){
      try{
         $statement = $db -> prepare($statement);
         $statement -> execute($variables);
         $result = $statement -> fetchAll(PDO::FETCH_ASSOC);
         if(is_array($result)){
            return $result;
         }
         else{
            return [];
         }
      }
      catch(Exception $e){
         return null;
      }
   }
   function insert($db,$statement,$variables){
      try{
         $statement = $db -> prepare($statement);
         $statement -> execute($variables);
         return true;
      }
      catch(Exception $e){
         return null;
      }
   }
   function in_column($db,$table,$column,$value){
      if(!is_array($column) && !is_array($value)){
         try{
            $statement = $db -> prepare('SELECT * FROM $table WHERE('.$column.' = :value);');
            if(!$statement -> execute([':value' => $value])){
               return null;
            }
            $result = $statement -> fetch(PDO::FETCH_ASSOC);
            if(!is_array($result)){
               return false;
            }
            else{
               return true;
            }
         }
         catch(Exception $e){
               return null;
         }
      }
      elseif(count($column) === count($value)){
         try{
            $column_count = count($column);
            $vars = [];
            $statement = 'SELECT * FROM '.$table.' WHERE(';
            for($i = 0;$i < $column_count;$i++){
               $statement .= $column[$i].' = ';
               $statement .= ':value'.$i;
               $vars[':value'.$i]  = $value[$i];
               if($i < $column_count - 1){
                  $statement .= ' AND ';
               }
            }
            $statement .= ');';
            $statement = $db -> prepare($statement);
            if(!$statement -> execute($vars)){
               return null;
            }
            $result = $statement -> fetch(PDO::FETCH_ASSOC);
            if(!is_array($result)){
               return false;
            }
            else{
               return true;
            }
         }
         catch(Exception $e){
            return null;
         }
      }
      else{
         return null;
      }
   }
?>