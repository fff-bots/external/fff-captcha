  <!DOCTYPE html>
  <html lang="de">
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Alle Daten</title>
      <style>
          *{
              margin: 0;
              padding: 0;
              box-sizing: border-box;
          }
          body{
              width: 100%;
              min-height: 100vh;
          }
          table{
              width: 100%;
              border-collapse: collapse;
          }
          th{
              text-align: left;
          }
          table, th, td{
              border: 0.1vw solid black;
          }
          th, td{
              padding: 1vh 1vw;
          }
          .red-bg{
              background: red;
              color: white;
          }
          .green-bg{
              background: green;
          }
          .not-matched{
              display: none;
          }
          input{
              width: 100%;
              padding: 0.5vh 0.5vw;
          }
          .counter{
              display: flex;
              justify-content: space-between;
              flex-wrap: wrap;
          }
          .counter div{
              padding: 1vh 1vw;
          }
          .insensitive-button-div{
              margin: 0 1vw 0 1vw;
              display: flex;
              justify-content: space-between;
          }
      </style>
      <script>
        class Search{
            constructor(haystack, search, caseInsensitiveCheck){
                this.searchInput = search;
                this.haystack = haystack;
                this.caseInsensitiveCheck = caseInsensitiveCheck;
                this.caseInsensitive = this.caseInsensitiveCheck.checked;
                this.caseInsensitiveCheck.addEventListener('click', () => {
                    this.caseInsensitive = this.caseInsensitiveCheck.checked;
                });
            }
            updateCaseInsensitive(){
                this.caseInsensitive = this.caseInsensitiveCheck.checked;
            }
            searchForValue(){
                let needle = this.searchInput.value;
                let result = [];
                Array.from(this.haystack).forEach((value, key) => {
                    let text = value.innerHTML;
                    if(this.caseInsensitive){
                        needle = needle.toLocaleLowerCase();
                        text = text.toLocaleLowerCase();
                    }
                    if(text.includes(needle)){
                        result[key] = true;
                    }
                    else{
                        result[key] = false;
                    }
                });
                return result;
            }
        }
        window.addEventListener('load', () => {
            let counter = document.createElement('div');
            counter.classList.add('counter');
            counter.innerHTML = `
            <div>Anzahl der angezeigten: ${document.querySelectorAll('tr').length - document.querySelectorAll('tr.not-matched').length - 1}</div>
            <div>Anzahl der angezeigten in Prozent: ${(((document.querySelectorAll('tr').length - document.querySelectorAll('tr.not-matched').length - 1) / (document.querySelectorAll('tr').length - 1)) * 100).toFixed(2)}%</div>
            <div>Anzahl der nicht angezeigten: ${(document.querySelectorAll('tr').length - 1) -(document.querySelectorAll('tr').length - document.querySelectorAll('tr.not-matched').length - 1)}</div>
            <div>Anzahl der nicht angezeigten in Prozent: ${(100 - (((document.querySelectorAll('tr').length - document.querySelectorAll('tr.not-matched').length - 1) / (document.querySelectorAll('tr').length - 1)) * 100)).toFixed(2)}%</div>`;
            const update = (search) => {
                rows.forEach((value) => {
                    value.dataset.unmatched = 'u';
                });
                searches.forEach((search) => {
                    search.searchForValue().forEach((value, key) => {
                        if(value === true){
                            if(rows[key].dataset.unmatched === 'u'){
                                rows[key].classList.remove('not-matched');
                            }
                        }
                        else{
                            rows[key].dataset.unmatched = 't';
                            rows[key].classList.add('not-matched');
                        }
                    });
                });
                counter.innerHTML = `
                <div>Anzahl der angezeigten: ${document.querySelectorAll('tr').length - document.querySelectorAll('tr.not-matched').length - 1}</div>
                <div>Anzahl der angezeigten in Prozent: ${(((document.querySelectorAll('tr').length - document.querySelectorAll('tr.not-matched').length - 1) / (document.querySelectorAll('tr').length - 1)) * 100).toFixed(2)}%</div>
                <div>Anzahl der nicht angezeigten: ${(document.querySelectorAll('tr').length - 1) -(document.querySelectorAll('tr').length - document.querySelectorAll('tr.not-matched').length - 1)}</div>
                <div>Anzahl der nicht angezeigten in Prozent: ${(100 - (((document.querySelectorAll('tr').length - document.querySelectorAll('tr.not-matched').length - 1) / (document.querySelectorAll('tr').length - 1)) * 100).toFixed(2))}%</div>`;
            };
            document.body.prepend(counter);
            let searches = [];
            let rows = Array.from(document.querySelectorAll('tr'));
            rows.shift();
            Array.from(rows[0].childNodes).forEach((value, key) => {
                let column = value.dataset.column;
                let search = document.createElement('input');
                let caseInsensitiveCheckBox = document.createElement('input');
                caseInsensitiveCheckBox.type = 'checkbox';
                caseInsensitiveCheckBox.classList.add('caseinsensitive-checkbox');
                let div = document.createElement('div');
                div.appendChild(document.createTextNode('Groß-/Kleinschreibung'));
                div.appendChild(caseInsensitiveCheckBox);
                document.querySelectorAll('th')[key].prepend(caseInsensitiveCheckBox);
                document.querySelectorAll('th')[key].prepend(search);
                let haystack = document.querySelectorAll(`td[data-column=${column}]`);
                if(document.querySelector(`td[data-column=${column}] a`) !== null){
                    let result = [];
                    Array.from(haystack).forEach((value) => {
                        let aTag = value.querySelector('a');
                        result.push(aTag || value);
                    });
                    haystack = result;
                }
                let searchObject = new Search(
                    haystack,
                    search,
                    caseInsensitiveCheckBox);
                caseInsensitiveCheckBox.addEventListener('click', () => {
                        update(searchObject);
                    });
                    searches.push(searchObject);
                });
            let insensitiveButtonDiv = document.createElement('div');
            insensitiveButtonDiv.classList.add('insensitive-button-div');
            document.body.prepend(insensitiveButtonDiv);
            let toggleAllInsenstiveButton = document.createElement('button');
            toggleAllInsenstiveButton.appendChild(document.createTextNode('Alle Groß-Kleinschreibung umdrehen'));
            toggleAllInsenstiveButton.addEventListener('click', () => {
                Array.from(document.querySelectorAll('.caseinsensitive-checkbox')).forEach((value) => {
                    value.checked = !value.checked;
                });
                searches.forEach((search) => {
                    search.updateCaseInsensitive();
                    update(search);
                });
            });
            insensitiveButtonDiv.appendChild(toggleAllInsenstiveButton);
            let unsetAllInsenstiveButton = document.createElement('button');
            unsetAllInsenstiveButton.appendChild(document.createTextNode('Alle Groß-Kleinschreib relevant'));
            unsetAllInsenstiveButton.addEventListener('click', () => {
                Array.from(document.querySelectorAll('.caseinsensitive-checkbox')).forEach((value) => {
                    value.checked = false;
                });
                searches.forEach((search) => {
                    search.updateCaseInsensitive();
                    update(search);
                });
            });
            insensitiveButtonDiv.appendChild(unsetAllInsenstiveButton);
            let setAllInsenstiveButton = document.createElement('button');
            setAllInsenstiveButton.appendChild(document.createTextNode('Alle Groß-Kleinschreib irrelevant'));
            setAllInsenstiveButton.addEventListener('click', () => {
                Array.from(document.querySelectorAll('.caseinsensitive-checkbox')).forEach((value) => {
                    value.checked = true;
                });
                searches.forEach((search) => {
                    search.updateCaseInsensitive();
                    update(search);
                });
            });
            insensitiveButtonDiv.appendChild(setAllInsenstiveButton);
            searches.forEach((search) => {
                search.searchInput.addEventListener('input', () => {
                    update(search.searchInput);
                });
            });
        });
      </script>
 </head>
 <body>
     <?php
         require_once('./sys-files/sql.php');
         $sql = sql_connect();
         if(is_null($sql)){
             die('Fehler in der Datenbankverbindung.');
         }
         $result = select($sql, 'SELECT ogs.og_name, ogs.og_state, ogs.og_email, links.link, links.group_name, links.active FROM ogs, links WHERE ogs.og_id = links.og_id',[]);
         if(is_null($result)){
             die('Fehler bei der Abfrage.');
         }
         if(empty($result)){
             die('Kein Eintrag gefunden');
         }
     ?>
     <table>
         <tr><th><div class="th-content">OG Name</div></th><th><div class="th-content">OG Bundesland</div></th><th><div class="th-content">OG Email</div></th><th><div class="th-content">Link</div></th><th><div class="th-content">Gruppenname</div></th><th><div class="th-content">Link Aktiv</div></th></tr>
         <?php
             $result = array_map(function($line){
                 $line['og_name'] = ucwords($line['og_name'], " \t\r\n\f\v-/\\");
                 $line['og_state'] = ucwords($line['og_state'], " \t\r\n\f\v-/\\");
                 if($line['og_email'] !== null){
                     $line['og_email'] = '<a href="mailto:'.$line['og_email'].'" target="_blank" rel="noreferrer noopener">'.$line['og_email'].'</a>';
                 }
                 if($line['active']){
                     $line['link'] = '<a href="'.$line['link'].'" target="_blank" rel="noreferrer noopener">'.$line['link'].'</a>';
                 }
                 return $line;
             }, $result);
             foreach($result as $line){
                 echo '<tr>';
                 foreach($line as $key => $entry){
                     if($key === 'active'){
                         if($entry === '0'){
                             echo '<td class="red-bg" data-column="'.$key.'">';
                             echo $entry;
                             echo '</td>';
                         }
                         else{
                             echo '<td class="green-bg" data-column="'.$key.'">';
                             echo $entry;
                             echo '</td>';
                         }
                     }
                     else{
                         echo '<td data-column="'.$key.'">';
                         echo $entry;
                         echo '</td>';
                     }
                 }
                 echo '</tr>';
             }
         ?>
     </table>
 </body>
 </html> 