<?php
   $path = 'https://docs.google.com/forms/';
   $data = http_build_query([
      'entry.1983410503_hour' => '15',
      'entry.1983410503_minute' => '18',
      'entry.1983410503_year' => '2020',
      'entry.1983410503_month' => '11',
      'entry.1983410503_day' => '14',
      'entry.338829644' => 'Allgemein',
      'entry.855716972' => 'Kian',
      'entry.338829644_sentinel' => '',
      'entry.855716972_sentinel' => '',
      'fvv' => '1',
      'draftResponse' => '[null,null,"-2215610947623140698"]'."\r\n",
      'fbzx' => '-2215610947623140698'
   ]);
   $opts = [
      'http' => [
         'header' => 
         "Content-Type: application/x-www-form-urlencoded\r\n"."Content-Length: ".strlen($data)."\r\n"."Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0\r\n",
         'method' => 'POST',
         'content' => $data,
         'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0'
      ]
   ];
   $context = stream_context_create($opts);
   echo file_get_contents($path,false,$context);
?>