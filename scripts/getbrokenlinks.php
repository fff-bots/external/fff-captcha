<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kaputte Gruppenlinks</title>
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        table{
            width: 100%;
            border-collapse: collapse;
        }
        th{
            text-align: left;
        }
        table, th, td{
            border: 0.1vw solid black;
        }
        th, td{
            padding: 1vh 1vw;
        }
    </style>
</head>
<body>
    <?php
        require_once('./sys-files/sql.php');
        $sql = sql_connect();
        if(is_null($sql)){
            die('Fehler in der Datenbankverbindung.');
        }
        $result = select($sql, 'SELECT ogs.og_name, ogs.og_state, ogs.og_email FROM ogs, links WHERE ogs.og_id = links.og_id AND links.active = 0',[]);
        if(is_null($result)){
            die('Fehler bei der Abfrage.');
        }
        if(empty($result)){
            die('Kein Eintrag gefunden');
        }
    ?>
    <table>
        <tr><th>OG Name</th><th>OG Bundesland</th><th>OG Email</th></tr>
        <?php
            foreach($result as $line){
                echo '<tr>';
                foreach($line as $entry){
                    echo '<td>';
                    echo $entry;
                    echo '</td>';
                }
                echo '</tr>';
            }
        ?>
    </table>
</body>
</html>