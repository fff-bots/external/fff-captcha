<?php
    $arrContextOptions = stream_context_create(["ssl"=>["verify_peer"=>false,"verify_peer_name"=>false]]);
    $link = 'https://api.fffutu.re/v1/localGroups/';
    $data = [];
    $data = json_decode(file_get_contents($link,false,$arrContextOptions),true);
    require_once('./sys-files/sql.php');
    $sql = sql_connect();
    if(!is_null($sql)){
        foreach($data as $key => $entry){
            if(!empty($entry['name']) && !empty($entry['state']) && (!empty($entry['whatsapp']))){
                if(empty($entry['email'])){
                   $entry['email'] = null;
                }
                $entry['name'] = mb_strtolower($entry['name']);
                $entry['state'] = mb_strtolower($entry['state']);
                if(!empty($entry['whatsapp'])){
                    require_once('./sys-files/messengerfunctions.php');
                    $groupName = get_whatsapp_name($entry['whatsapp']);
                    if(empty($groupName)){
                        $active = 0;
                    }
                    else{
                        $active = 1;
                    }
                    if(in_column($sql,'ogs',['og_name','og_state'],[$entry['name'],$entry['state']]) === true){
                        if(in_column($sql,'ogs',['og_name','og_state','og_email'],[$entry['name'],$entry['state'],$entry['email']]) !== true){
                            insert($sql,'UPDATE ogs SET(og_email = :og_email) WHERE(og_name = :og_name AND og_state = :og_state)',[':og_email' => $entry['email'],':og_name' => $entry['name'],':og_state' => $entry['state']]);
                        }
                    }
                    else{
                        insert($sql,'INSERT INTO ogs (og_name,og_state,og_email) VALUES(:og_name,:og_state,:og_email)',[':og_name' => $entry['name'],':og_state' => $entry['state'],':og_email' => $entry['email']]);
                    }
                    $og_id = @select($sql,'SELECT * FROM ogs WHERE(og_name = :og_name AND og_state = :og_state)',[':og_name' => $entry['name'],':og_state' => $entry['state']])[0]['og_id'];
                    if(in_column($sql,'links',['og_id','type'],[$og_id,'whatsapp']) === true){
                        if(in_column($sql,'links',['og_id','type','link','active','group_name'],[$og_id,'whatsapp',$entry['whatsapp'],$active,$groupName]) !== true){
                            insert($sql,'UPDATE links SET(link = :link,active = :active,group_name = :group_name) WHERE(og_id = :og_id AND type = :type)',[':og_id' => $og_id,':type' => 'whatsapp',':link' => $entry['whatsapp'],':active' => $active,':group_name' => $groupName]);
                        }
                    }
                    else{
                        insert($sql,'INSERT INTO links(og_id,type,link,active,group_name) VALUES(:og_id,:type,:link,:active,:group_name)',[':og_id' => $og_id,':type' => 'whatsapp','link' => $entry['whatsapp'],':active' => $active,':group_name' => $groupName]);
                    }
                }
            }
        }
    }
    else{
        die('Datenbank Verbindung Fehlgeschlagen');
    }
?> 