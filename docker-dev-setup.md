# Setup docker dev setup
To setup a local dev setup via docker you only need docker & docker compose.

1. you have to change the $server in web/sys-files/sql.php line 4 to `db` or simply apply this patch:

```patch
diff --git a/web/sys-files/sql.php b/web/sys-files/sql.php
index 6feb8b6..bdfaf47 100644
--- a/web/sys-files/sql.php
+++ b/web/sys-files/sql.php
@@ -1,7 +1,7 @@
 <?php
     function sql_connect(){
         try{
-            $server = 'localhost';
+            $server = 'db';
             $user = 'root';
             $password = '';
             $database = 'messengerag';
```

2. you have to obtain a dump of the database and put it into the dump/ folder (docker will later import it automatically)

3. finally just run `sudo docker-compose up -d` and go to [http://localhost:8080/](http://localhost:8080/) where the website should be running now

4. just do your edits in the folder itself and they will automatically appear after a reload

5. have fun contributing
