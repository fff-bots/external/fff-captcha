<?php
   function generateButtonName(int $length){
      $password = '';
      $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      str_shuffle($chars);
      for($i = 0; $i < $length; $i++){
          $password .= $chars[random_int(0,strlen($chars)-1)];
      }
      return $password;
   }
   function generateRandomButton(){
      $name = generateButtonName(rand(5,10));
      $content = generateButtonName(rand(5,10));
      $button = '<button';
      $button .= ' name="'.$name.'">'.$content.'</button>';
      return $button;
   }
   $display = 0;
   if(!empty($_POST) && empty($_POST['validate'])){
      header('Location: https://chat.whatsapp.com/LwNqoPCHnKWBijnassqCDz');
      exit;
   }
   if(!empty($_GET['og']) && !empty($_GET['type'])){
      $_GET['og'] = str_replace('+',' ',urldecode($_GET['og']));
      $_GET['type'] = str_replace('+',' ',urldecode($_GET['type']));
      $display = 1;
      require_once('sys-files/sql.php');
      $sql = sql_connect();
      if(!is_null($sql)){
         $statement = $sql -> prepare('SELECT * FROM ogs, links WHERE(links.og_id = ogs.og_id AND links.type = :type AND ogs.og_name = :og_name);');
         if(!$statement -> execute([':og_name' => $_GET['og'],':type' => $_GET['type']])){
            $display = 2;
         }
         $result = $statement -> fetch(PDO::FETCH_ASSOC);
         if(is_array($result)){
            if(!empty($_POST['validate'])){
               if($result['active'] == 1){
                  header('Location: '.$result['link']);
                  exit;
               }
               else{
                  $display = 5;
               }
            }
            else{
               $display = 1;
            }
         }
         else{
            $display = 3;
         }
      }
      else{
         $display = 4;
      }
   }
   if($display == 5){
      $error = 'Diese OG ist vorübergehend nicht erreichbar. Bitte versuche es zu einem späterem Zeitpunkt erneut.';
      $display = 1;
   }
   if($display == 4){
      require_once('./sys-files/error-pages/500.php');
   }
   if($display == 3){
      require_once('./sys-files/error-pages/404.php');
   }
   if($display == 2){
      require_once('./sys-files/error-pages/500.php');
   }
   if($display == 1){
      $title = 'Weiterleiten zu '.ucwords($_GET['og']);
      $range = 0;
      $nav = ['Kontakt zur Messenger AG','Fehler melden','Datenschutz'];
      $mainClass = 'captcha-main';
      require_once('./sys-files/header.php');
      echo '<h1>Bist du ein Bot?</h1>';
      echo '<form action="'.(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http").'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'" method="post" class="captcha">';
      if(!empty($error)){
         echo '<span class="error">'.htmlspecialchars($error).'</span>';
      }
      echo '<button name="redirect" value="1">Redirect</button>';
      for($i = 0; $i < rand(9,12); $i++){
         echo generateRandomButton();
      }
      echo '<button name="validate" class="button" value="1">Ich? Ein Bot? Ne.</button>';
      for($i = 0; $i < rand(9,12); $i++){
         echo generateRandomButton();
      }
      echo '<div id="bot"><img src="/img/icon/Robot-256-white.svg"><span>Beep.</span></div>';
      echo '</form>';
      
      require_once('./sys-files/footer.php');
   }
   if($display == 0){
      require_once('./sys-files/error-pages/404.php');
   }
?>
<script>
   let bot = document.querySelector('#bot');
   let botListener = bot.addEventListener('click',() => {
      bot.innerHTML = 'Glaube ich nicht.';
      bot.removeEventListener('click',botListener);
      bot.classList.add('toggled');
   });
   function botToggle(){
      bot.innerHTML = 'Glaube ich nicht.';
   }
</script>