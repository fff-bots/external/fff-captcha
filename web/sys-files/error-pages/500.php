<?php
   $title = 'Error 500, Interner Server Error';
   $mainClass = 'error-page 500';
   $range = 0;
   $nav = ['Fehler melden','Kontakt zur Messenger AG','Datenschutz'];
   require_once(__DIR__.'/../header.php');
?>
<h1>Error. 500</h1>
<h2>Diese Seite ist wirklich sicher vor Bots.</h2>
<p>
Denn sie funktioniert nicht. Bitte nutze einen anderen Link ;P
</p>
<img src="/img/graphics/robot.svg" alt="Roboter">
<?php
   require_once(__DIR__.'/../footer.php');
?>