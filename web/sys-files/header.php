<!DOCTYPE html>
<html lang="de">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, min-width=device-width">
   <title><?php if(!empty($title)){echo $title;} ?></title>

   <link rel="stylesheet" type="text/css" href="/css/stylesheet.css">
   <link rel="stylesheet" type="text/css" href="/css/jost.css">

   <meta name="robots" content="noindex">

   <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
   <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
   <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
   <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
   <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
   <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
   <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
   <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
   <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
   <link rel="icon" type="image/png" sizes="192x192" href="/img/favicon/android-icon-192x192.png">
   <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
   <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
   <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
   <link rel="manifest" href="/img/favicon/manifest.json">
   <meta name="msapplication-TileColor" content="#ffffff">
   <meta name="msapplication-TileImage" content="/img/favicon/ms-icon-144x144.png">
   <meta name="theme-color" content="#ffffff">

   <?php
      if(!empty($head_tags)){
         foreach($head_tags as $head_tag){
            echo $head_tag;
         }
      }
   ?>

</head>
<body>
   <header>
      <input type="checkbox" id="navtoggle">
      <label for="navtoggle" id="navbox">
         <span></span>
         <span></span>
         <span></span>
      </label>
      <span class="url">Captcha.fridaysforfuture.dev</span>
      <?php
         if(isset($range) && !empty($mainClass)){
            if($range == 0 && $mainClass != 'failure-main'){
               echo '<a href="/fehler-melden.php" title="Klicke hier, wenn die Weiterleitung nicht funktioniert.">Fehler melden</a>';
            }
            else{
               echo '<span class="placeholder">If you can read this, you might be a bot.</span>';
            }
         }
         else{
            echo '<span class="placeholder">If you can read this, you might be a bot.</span>';
         }
      ?>
      <nav>
         <span class="placeholder">If you can read this, you might be a bot.</span>
      <ul>
         <?php
            $nav_links = ['Kontakt zur Messenger AG' => '/kontakt.php','Fehler melden' => '/fehler-melden.php','Datenschutz' => '/datenschutz.php'];
            foreach($nav as $nav_item){
               if(!empty($nav_links[$nav_item])){
                  $nav_link = $nav_links[$nav_item];
                  echo '<li>';
                  echo '<a href="'.htmlspecialchars($nav_link).'">'.htmlspecialchars($nav_item).'</a>';
                  echo '</li>';
               }
            }
         ?>
      </ul>
      </nav>
   </header>
   <main <?php if(!empty($mainClass)){echo 'class="'.$mainClass.'"';}?>>