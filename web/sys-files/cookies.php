<div class="cookie-wrapper">
   <div class="cookie-information">
      <p>
         Diese Website verwendet Cookies. Allerdings nur technisch notwendige.
      </p>
      <button id="cookie-accept">Technisch notwendige Cookies aktzeptieren</button>
   </div>
</div>
<script>
   document.querySelector('body').classList.add('cookie-overlay');
   document.querySelector('#cookie-accept').addEventListener('click', () => {
      document.cookie = "cookie-allow=true;max-age=2592000;SameSite=strict;";
      document.querySelector('body').classList.remove('cookie-overlay');
      document.querySelector('.cookie-wrapper').remove();
   });
</script>