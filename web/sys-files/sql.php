<?php
    function sql_connect(){
         try{
            require(__DIR__.'/config.php');
            $server = $mysql_server;
            $user = $mysql_user;
            $password = $mysql_password;
            $database = 'fff_captcha';
            $db = new PDO('mysql:host='.$server.';dbname='.$database,$user,$password);
            return $db;
         }
         catch(Exception $e){
            return null;
         }
    }
    function select($db,$statement,$variables){
        try{
            $statement = $db -> prepare($statement);
            $statement -> execute($variables);
            $result = $statement -> fetchAll(PDO::FETCH_ASSOC);
            if(is_array($result)){
                return $result;
            }
            else{
                return [];
            }
        }
        catch(Exception $e){
            return null;
        }
    }
    function insert($db,$statement,$variables){
        try{
        $statement = $db -> prepare($statement);
        $statement -> execute($variables);
        return true;
        }
        catch(Exception $e){
            return null;
        }
    }
    function in_column($db,$table,$column,$value){
        try{
            $statement = $db -> prepare("SELECT * FROM $table WHERE($column = :value);");
            if(!$statement -> execute([':value' => $value])){
            }
            $result = $statement -> fetch(PDO::FETCH_ASSOC);
            if(!is_array($result)){
                return false;
            }
            else{
                return true;
            }
        }
        catch(Exception $e){
            return null;
        }
    }
?>