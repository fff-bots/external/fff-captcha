<?php
   #name this file config.php
   $mysql_password = '';
   $mysql_user = 'root';
   $mysql_server = 'localhost';
   $link_umfrage = 'https://docs.google.com/forms/u/0/d/e/1FAIpQLSccUZI_RxkjUtz_ILKh34pcjp1wcB3sGsmOQku9DZUFIF7FUQ/formResponse';
   $umfrage_ids = [
      'name' => 'entry.453822406',
      'state' => 'entry.519735625',
      'whatsapp' => 'entry.832587050',
      'telegram' => 'entry.1808089348',
      'instagram' => 'entry.995519465',
      'facebook' => 'entry.110868127',
      'twitter' => 'entry.925048126',
      'email' => 'entry.393635778',
      'sonstiges' => 'entry.78786734',
      'kontakt' => 'entry.989606538',
      'action' => 'entry.1872227888'
   ];
?>