<?php
   $title = 'Kontakt zur Messenger AG';
   $mainClass = 'contact';
   $nav = ['Fehler melden','Datenschutz'];
   $range = 0;
   require_once('./sys-files/header.php');
?>
<h1>Contact. us.</h1>
<?php
   $display = 0;
   if(!empty($_POST['email']) && !empty($_POST['content'])){
      if(!empty($_POST['conditions'])){
         if(filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
            mail('Finn_luka@gmx.de','Kontakt zur Messenger AG Captcha',$_POST['content'],['FROM' => $_POST['email']]);
            $display = 1;
         }
         else{
            $error = 'Deine Email-Adresse ist nicht gültig.';
         }
      }
      else{
         $error = 'Du musst die Nutzungsbedingungen aktzeptieren';
      }
   }
   if($display == 1){
      echo '<div class="feedback"><div class="success">Vielen Dank für deine Nachricht.</div></div>';
   }
   if($display == 0){
      echo '<form class="feedback" method="post" action="'.(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http").'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'">';
      echo '<span class="error">';
      if(!empty($error)){
         echo $error;
      }
      echo '</span>';
      echo '<input type="email" name="email" placeholder="Deine Mailadresse" required content="';
      if(!empty($_POST['email'])){
         echo htmlspecialchars($_POST['email']);
      }
      echo '">';
      echo '<textarea name="content" placeholder="Beschreibe den Fehler hier." required>';
      if(!empty($_POST['content'])){
         echo htmlspecialchars($_POST['content']);
      }
      echo '</textarea>';
      echo '<div class="conditions">';
      echo '<input type="checkbox" name="conditions" value="1" id="conditions" class="switch-toggle">';
      echo '<label for="conditions" class="switch"><div class="switch-bg"><div class="switch-circle-bg"><div class="switch-circle"></div></div></div><span class="switch-text">Ich habe die <a href="'.$nav_links['Datenschutz'].'">Datenschutzerklärung</a> zur Kenntnis genommen, und bin damit einverstanden.</span></label>';
      echo '</div>';
      echo '<button>Senden</button>';
      echo '</form>';
   }
?>
<?php
   require_once('./sys-files/footer.php');
?>