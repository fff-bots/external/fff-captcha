<?php
   $display = 0;
   require_once('./sys-files/accountfunctions.php');
   $cookie_allow = accept_cookies();
   if($cookie_allow){
      if(!empty($_POST['username']) && !empty($_POST['password'])){
         $_POST['username'] = strtolower($_POST['username']);
         require_once('./sys-files/sql.php');
         $sql = sql_connect();
         if(!is_null($sql)){
            $user_data = select($sql,'SELECT * FROM user WHERE username = :username',[':username' => $_POST['username']]);
            if(!empty($user_data)){
               $user_data = $user_data[0];
               if(password_verify($_POST['password'],$user_data['password'])){
                  session_start();
                  $_SESSION['user_data'] = $user_data;
                  header('Location: /admin/verwaltung.php');
               }
               else{
                  $error = 'Nutzername oder Passwort falsch.';
               }
            }
            else{
               $error = 'Nutzername oder Passwort falsch.';
            }
         }
         else{
            $error = 'Ein unerwarteter Fehler ist aufgetreten. Bitte versuche es später erneut.';
         }
      }
      else{
         if(!empty($_POST['username']) || !empty($_POST['password'])){
            $error = 'Du musst Nutzername und Passwort angeben!';
         }
      }
   }
   else{
      $error = 'Du musst technisch notwendige Cookies aktzeptieren.';
   }
   if($display == 0){
      $title = 'Log-In';
      $mainClass = 'login';
      $nav = ['Fehler melden','Kontakt zur Messenger AG','Datenschutz'];
      $range = 0;
      $head_tags = ['<link rel="stylesheet" type="text/css" href="/admin/css/admin.css">'];
      require_once('./sys-files/header.php');
?>
<form class="login-form" action="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http").'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>" method="post">
   <?php
      if(!empty($error)){
         echo '<span class="error">'.$error.'</span>';
      }
   ?>
   <input type="text" name="username" placeholder="Nutzername" <?php
      if(!empty($_POST['username'])){
         echo 'value="'.$_POST['username'].'" ';
      }
      ?>required>
   <input type="password" name="password" placeholder="Passwort" required>
   <button>Los</button>
</form>
<?php
      require_once('./sys-files/footer.php');
   }
?>