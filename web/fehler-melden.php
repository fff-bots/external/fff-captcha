<?php
   $title = 'Fehler melden';
   $nav = ['Kontakt zur Messenger AG','Datenschutz'];
   $mainClass = 'failure-main';
   require_once('./sys-files/header.php');
?>
<h1>Melde einen Fehler</h1>
<?php
   $display = 0;
   if(!empty($_POST['email']) && !empty($_POST['content'])){
      if(!empty($_POST['conditions'])){
         if(filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
            mail('Finn_luka@gmx.de','Fehlermeldung Captcha',$_POST['content'],['FROM' => $_POST['email']]);
            $display = 1;
         }
         else{
            $error = 'Deine Email-Adresse ist nicht gültig.';
         }
      }
      else{
         $error = 'Du musst die Nutzungsbedingungen aktzeptieren';
      }
   }
   if($display == 1){
      echo '<div class="feedback"><div class="success">Vielen Dank für deine Nachricht.</div></div>';
   }
   if($display == 0){
?>
      <form class="feedback" method="post" action="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http").'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>">
      <span class="error">
<?php
      if(!empty($error)){
         echo $error;
      }
?>
      </span>
      <input type="email" name="email" placeholder="Deine Mailadresse" required content="
<?php
      if(!empty($_POST['email'])){
         echo htmlspecialchars($_POST['email']);
      }
?>">
      <textarea name="content" placeholder="Beschreibe den Fehler hier." required>
<?php
      if(!empty($_POST['content'])){
         echo htmlspecialchars($_POST['content']);
      }
?>
      </textarea>
      <div class="conditions">
      <input type="checkbox" name="conditions" value="1" id="conditions" class="switch-toggle">
      <label for="conditions" class="switch"><div class="switch-bg"><div class="switch-circle-bg"><div class="switch-circle"></div></div></div><span class="switch-text">Ich habe die <a href="<?php echo $nav_links['Datenschutz'];?>">Datenschutzerklärung</a> zur Kenntnis genommen, und bin damit einverstanden.</span></label>
      </div>
      <button>Senden</button>
      </form>
<?php
   }
?>
<?php
   require_once('./sys-files/footer.php');
?>