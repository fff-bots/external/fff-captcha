<?php
   $display = 0;
   $type = 'whatsapp';
   require_once('../sys-files/accountfunctions.php');
   if(accept_cookies()){
      session_start();
      if(logged_in()){
         require_once('../sys-files/sql.php');
         $sql = sql_connect();
         if(!is_null($sql)){
            require_once('./sys-files/messengerfunctions.php');
            if(!empty($_POST['og-name'])){
               $_POST['og-name'] = mb_strtolower($_POST['og-name']);
            }
            if(!empty($_POST['action']) && !empty($_POST['og-name'])){
               switch($_POST['action']){
                  case 1:
                     if(in_column($sql,'ogs','og_name',$_POST['og-name']) !== true){
                        $og_data = @json_decode(file_get_contents('https://api.fffutu.re/v1/localGroups/?name='.urlencode($_POST['og-name'])),true);
                        if(!empty($og_data)){
                           $og_data = $og_data[0];
                           if(!empty($og_data['name']) && !empty($og_data['state'])){
                              $og_data['name'] = mb_strtolower($og_data['name']);
                              $og_data['state'] = mb_strtolower($og_data['state']);
                              if(empty($og_data['email'])){
                                 $og_data['email'] = null;
                              }
                              if(insert($sql,'INSERT INTO ogs (og_name,og_state,og_email) VALUES(:og_name,:og_state,:og_email)',[':og_name' => $og_data['name'],':og_state' => $og_data['state'],':og_email' => $og_data['email']])){
                                 $display = 0;
                                 $success = 'Die OG '.htmlspecialchars(ucwords($og_data['name'])).' wurde erfolgreich erstellt. Bitte füge einen neu generierten Link ein.';
                                 $og_id = @select($sql,'SELECT * FROM ogs WHERE(og_name = :og_name)',[':og_name' => $og_data['name']])[0]['og_id'];
                                 if(empty($og_id)){
                                    unset($og_id);
                                    $display = 0;
                                    $error = 'Ein unerwarteter Fehler ist aufgetreten, bite versuche es später erneut.';
                                    break;
                                 }
                              }
                           }
                           else{
                              $display = 0;
                              $error = 'Diese OG scheint falsche Daten zu haben.';
                              break;
                           }
                        }
                        else{
                           $display = 0;
                           $error = 'Diese OG scheint nicht zu existieren.';
                           break;
                        }
                     }
                     else{
                        $og_id = @select($sql,'SELECT * FROM ogs WHERE(og_name = :og_name)',[':og_name' => $_POST['og-name']])[0]['og_id'];
                        if(empty($og_id)){
                           unset($og_id);
                           $display = 0;
                           $error = 'Ein unerwarteter Fehler ist aufgetreten, bite versuche es später erneut.';
                           break;
                        }
                        else{
                           if(in_column($sql,'links','og_id',$og_id) === true){
                              if(insert($sql,'UPDATE links SET active = :active WHERE og_id = :og_id',[':active' => 1,':og_id' => $og_id]) === true){
                                 $display = 0;
                                 $success = 'Der Link wurde erfolgreich aktiviert.';
                                 break;
                              }
                              else{
                                 $display = 0;
                                 $error = 'Ein unerwarteter Fehler ist aufgetreten. Bitte versuche es später erneut.';
                                 break;
                              }
                           }
                        }
                     }
                     $success = 'Bitte lege einen neuen Whatsapp Link an.';
                  break;
                  case 2:
                     if(in_column($sql,'ogs','og_name',$_POST['og-name']) === true){
                        $og_id = @select($sql,'SELECT * FROM ogs WHERE(og_name = :og_name)',[':og_name' => $_POST['og-name']])[0]['og_id'];
                        if(!empty($og_id)){
                           if(in_column($sql,'links','og_id',$og_id)){
                              if(insert($sql,'UPDATE links SET active = :active WHERE(og_id = :og_id)',[':active' => 0,':og_id' => intval($og_id)]) === true){
                                 $display = 0;
                                 $success = 'Die OG wurde erfolgreich abgeschaltet.';
                                 break;
                              }
                              else{
                                 $display = 0;
                                 $error = 'Ein unerwarteter Fehler ist aufgetreten. Bitte versuche es später erneut.';
                                 break;
                              }
                           }
                           else{
                              $display = 0;
                              $error = 'Diese OG hat keine Gruppen.';
                              break;
                           }
                        }
                        else{
                           $display = 0;
                           $error = 'Ein unerwarteter Fehler ist aufgetreten. Bitte versuche es später ernaut.';
                           break;
                        }
                     }
                     else{
                        $display = 0;
                        $error = 'Diese OG ist nicht eingetragen.';
                        break;
                     }
                  break;
                  case 3:
                     if(!empty($_POST['og-link'])){
                        if(in_column($sql,'ogs','og_name',$_POST['og-name']) === true){
                           $og_id = @select($sql,'SELECT * FROM ogs WHERE(og_name = :og_name)',[':og_name' => $_POST['og-name']])[0]['og_id'];
                           if(!empty($og_id)){
                              if(in_column($sql,'links','link',$_POST['og-link']) === false){
                                 $api = @json_decode(file_get_contents('https://api.fffutu.re/v1/localGroups/'),true);
                                 if(!empty($api)){
                                    $on_website = false;
                                    foreach($api as $og){
                                       if(!$on_website){
                                          if(!empty($og['whatsapp'])){
                                             if($og['whatsapp'] === $_POST['og-link']){
                                                $on_website = true;
                                                break;
                                             }
                                          }
                                       }
                                    }
                                    if(!$on_website){
                                       $group_name = get_whatsapp_name($_POST['og-link']);
                                       if(!empty($group_name)){
                                          if(in_column($sql,'links','og_id',$og_id) === true){
                                             if(insert($sql,'UPDATE links SET link = :link WHERE(og_id = :og_id)',[':og_id' => $og_id,':link' => $_POST['og-link']]) === true){
                                                $display = 0;
                                                $success = 'Der Link wurde erfolgreich geändert. Der Gruppenname lautet: "'.htmlspecialchars($group_name).'"';
                                                break;
                                             }
                                             else{
                                                $display = 0;
                                                $error = 'Ein unerwarteter Fehler ist aufgetreten, bitte versuche es später erneut.';
                                                break;
                                             }
                                          }
                                          else{
                                             if(insert($sql,'INSERT INTO links (og_id,type,link,group_name,active) VALUES(:og_id,:type,:link,:group_name,:active)',[':og_id' => $og_id,':type' => $type,':link' => $_POST['og-link'],':group_name' => $group_name,':active' => 1])){
                                                $display = 0;
                                                $success = 'Der Link wurde erfolgreich erstellt. Der Gruppenname lautet: "'.htmlspecialchars($group_name).'"';
                                                require_once('./sys-files/webagform.php');
                                                $og_data = select($sql,'SELECT * FROM ogs WHERE og_id = :og_id',[':og_id' => $og_id]);
                                                if(!empty($og_data)){
                                                   $og_data = $og_data[0];
                                                   if(edit_og(['name' => $_POST['og-name'],'state' => $og_data['og_state'],'kontakt' => '+49 1590 1766743 im Auftrag der OG '.ucwords($_POST['og-name']).'.','whatsapp' => 'https://captcha.fridaysforfuture.dev/'.urlencode(mb_strtolower($_POST['og-name'])).'/whatsapp/'],false)){
                                                      $display = 0;
                                                      $success = 'Der Link wurde erfolgreich erstellt und auf der Website eingetragen. Der Gruppenname lautet: "'.htmlspecialchars($group_name).'"';
                                                      break;
                                                   }
                                                   else{
                                                      $display = 0;
                                                      $error = 'Die Gruppe wurde erfolgreich erstellt, ist aber noch nicht auf der Website. Bitte versuche es später erneut, oder fügr den Link manuell ein.';
                                                      break;
                                                   }
                                                }
                                                else{
                                                   $display = 0;
                                                   $error = 'Ein unerwarteter Fehler ist aufgetreten. Bitte versuche es später erneut.';
                                                   break;
                                                }
                                             }
                                             else{
                                                $display = 0;
                                                $error = 'Ein unerwarteter Fehler ist aufgetreten. Bitte versuche es später erneut.';
                                                break;
                                             }
                                          }
                                       }
                                       else{
                                          $display = 0;
                                          $error = 'Der Link scheint nicht zu funktionieren.';
                                          break;
                                       }
                                    }
                                    else{
                                       $display = 0;
                                       $error = 'Dieser Link ist bereits auf der Website eingetragen. Bitte generiere einen neuen Link.';
                                       break;
                                    }
                                 }
                                 else{
                                    $display = 0;
                                    $error = 'Ein unerwarteter Fehler ist aufgetreten. Bitte versuche es später erneut.';
                                 }
                              }
                              else{
                                 $display = 0;
                                 $error = 'Dieser Link ist bereits eingetragen.';
                              }
                           }
                           else{
                              $display = 0;
                              $error = 'Ein unerwarteter Fehler ist aufgetreten. Bitte versuche es später erneut.';
                              break;
                           }
                        }
                        else{
                           $display = 0;
                           $error = 'Diese OG scheint nicht registriert zu sein oder nicht zu existieren.';
                           break;
                        }
                     }
                     else{
                        $display = 0;
                        $error = 'Du musst einen Link angeben!';
                        break;
                     }
                  break;
                  default:
                     $display = 0;
                     $error = 'Invalide Aktion.';
                  break;
               }
            }
            else{
               if(!empty($_POST['action']) || !empty($_POST['og-name'])){
                  $error = 'Du musst eine Aktion und eine OG eingeben.';
               }
            }
         }
         else{
            require_once('../sys-files/error-pages/500.php');
         }
      }
      else{
         header('Location: /login.php');
      }
   }
   else{
      header('Location: /login.php');
   }
   if($display == 0){
      $title = 'Verwaltung';
      $mainClass = 'verwaltung';
      $range = 0;
      $nav = ['Fehler melden','Kontakt zur Messenger AG','Datenschutz'];
      $head_tags = ['<link rel="stylesheet" type="text/css" href="/admin/css/admin.css">'];
      require_once('../sys-files/header.php');
?>
<h1>Verwaltung</h1>
<div class="action">
      <h2>Was möchtest du machen?</h2>
      <form class="verwaltung-action" action="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http").'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>" method="post">
         <?php
            if(!empty($error)){
               echo '<span class="error">'.htmlspecialchars($error).'</span>';
            }
            elseif(!empty($success)){
               echo '<span class="success">'.htmlspecialchars($success).'</span>';
            }
         ?>
         <select name="action" id="action">
            <option value="1">OG freischalten</option>
            <option value="2">OG abschalten</option>
            <option value="3">Link ändern</option>
         </select>
         <input type="text" name="og-name" placeholder="OG-Name" required autofocus>
         <input type="url" name="og-link" id="group-link" placeholder="Einladungslink">
         <button>OK</button>
      </form>
      <script>
         const action = document.querySelector('#action');
         const inputLink = document.querySelector('#group-link');
         action.addEventListener('change', () => {
            if(action.value == 3){
               inputLink.style.display = 'initial';
               inputLink.required = true;
            }
            else{
               inputLink.style.display = 'none';
               inputLink.required = false;
            }
         });
      </script>
</div>
<?php
      require_once('../sys-files/footer.php');
   }
?>