<?php
   function edit_og($params,$new = false){
      try{
         require(__DIR__.'/../../sys-files/config.php');
         $link = $link_umfrage;
         $ids = $umfrage_ids;
         if(!empty($params['action'])){
            unset($params['action']);
         }
         $data = [];
         if(!empty($params['name']) && !empty($params['state']) && !empty($params['kontakt'])){
            foreach($params as $key => $param){
               if(empty($ids[$key])){
                  unset($params[$key]);
               }
               else{
                  $data[$ids[$key]] = $param;
               }
            }
            if($new){
               $data[$ids['action']] = 'Neue Regionalgruppe';
            }
            else{
               $data[$ids['action']] = 'Link zum aktuallisieren';
            }
            $query = http_build_query($data);
            $opts = [
               'http' => [
                  'header' => 
                  "Content-Type: application/x-www-form-urlencoded\r\nContent-Length: ".strlen($query)."\r\nMozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0\r\n",
                  'method' => 'POST',
                  'content' => $query,
                  'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0'
               ]
            ];
            $context = stream_context_create($opts);
            file_get_contents($link,false,$context);
            return true;
         }
      }
      catch(Exception $e){
         return null;
      }
   }
?>